#lang slideshow
(require  pict-abbrevs)
(require slideshow/text)
(require "../common/orgmode-transforms.rkt")
(require "../common/pict-procedures.rkt")
(require "../common/ps-draw-procedures.rkt") ;; for pict->page .ps file
(require "../common/qrcode-imgs.rkt")
;; #lang racket
;; (require pict)

;; Osprey
(define levels-ppms-osprey (list
                    '("water" 0.000003)
                    '("zooplankton" 0.04)
                    '("small-fish" 0.5)
                    '("large-fish" 2)
                    '("osprey" 25)))
(define levels-osprey (map first levels-ppms-osprey))
(define ppms-osprey (map second levels-ppms-osprey))
(match-define (list mp1-o mp2-o mp3-o mp4-o mp5-o)
  ppms-osprey)
;; (/ mp2-o mp1-o) ; ok!
(define (get-concent-mult lst) ; multiple? x times? 
  (define start-ppm(first lst))
  (define (start-divn ppm)
    (/ ppm start-ppm))
  (map start-divn lst))
(define concent-times-o (get-concent-mult ppms-osprey)) ;; concent-times-o

;; Grebes, Clear Lake, Silent Spring 
;; (define levels-ppms-g

;; Herring Gulls
(define levels-ppms-h (list
                       '("water" 0.0001)
                       '("phytoplankton" 0.025)
                       '("zooplankton" 0.123)
                       '("smelt" 1.04)
                       '("lake-trout" 4.83)
                       '("gull-eggs" 124)))
(define lvls-h (map first levels-ppms-h))
(define ppms-h (map second levels-ppms-h))
(define (get-square-side lst)
  (sqrt (/ (last lst) (first lst))))
(define sqr-sde-h (get-square-side ppms-h))
;; (map second levels-ppms-h)))
(define (make-sqr-scl-bg lst (clr "gray"))
  (define sqr-len (get-square-side lst))
  (filled-rectangle sqr-len sqr-len #:color clr))
(define sqr-scl-bg-h (make-sqr-scl-bg ppms-h))
;; (pict->png-n-page sqr-scl-bg-h "square-scale-herring")
(define concent-times-h (get-concent-mult ppms-h))
;; concent-times-h
(define clr-lst (list "sea green" "deep sky blue" "aqua" "light gray" "gray" "dim gray"))
(define (make-ppm-sqr ppm clr)
  (define len (sqrt ppm))
  (filled-rectangle len len #:color clr #:draw-border? #f))
(define sqrs-h (map make-ppm-sqr concent-times-h clr-lst))
;; (p-v-r (apply vl-append (take sqrs-h 4))) ;; ok!
(define (sqrs-impsr lst (impsr lt-superimpose)) ;; lb-superimpose
  (define rev-lst (reverse lst))                    
  (define (helper l p)
    (cond
      ((empty? l) p)
      (#t (helper (cdr l) (impsr p (car l))))))
  (helper (cdr  rev-lst) (car rev-lst)))
(define concentration-times-sqrs-h (sqrs-impsr sqrs-h))
(define (sqrs-cntrr lst)
  (define (helper l p)
    (cond
      ((empty? l) p)
      (#t (helper (cdr l) (panorama (pin-to-center p (first l)))))))
  (helper (reverse lst) (blank)))
(define concentration-sqrs-cntrd-h (sqrs-cntrr sqrs-h))
;; (pict->png-n-page concentration-sqrs-cntrd-h "Concent-squares-cntrd-gull") ; ok!

(define (make-clr-key lbl clr (len 25) (lbl-clr "crimson")) ; pink
  (define l-p (t lbl))
  (define c-p (filled-rectangle len len #:color clr #:border-color lbl-clr #:border-width 2))
  (hc-append 20 c-p l-p))
(define clr-key-h (add-bg
                   (apply vl-append 15 (map make-clr-key lvls-h clr-lst)))) ;; clr-key-h ; ok!
(define conc-sgrs-cntr-h-w-clr-key
  ;;(pin-over concentration-sqrs-cntrd-h 20 20 clr-key-h))
  (lt-superimpose concentration-sqrs-cntrd-h clr-key-h))

(pict->png-n-page concentration-sqrs-cntrd-h "Bio-Accum-herring-gull-eggs")

(define clrs-lst-o (list "light blue" "aqua" "light gray" "gray" "dim gray"))
(define (make-clr-panel lvls clrs)
  (add-bg (apply vl-append 15 (map make-clr-key lvls clrs))))
(define clr-key-o (make-clr-panel levels-osprey clrs-lst-o))
;; clr-key-o
(define (make-concentration-comparison-squares ppms-lst clrs-lst)
  (sqrs-impsr (map make-ppm-sqr (get-concent-mult ppms-lst) clrs-lst)))
(define concentration-times-sqrs-o
  (make-concentration-comparison-squares ppms-osprey clrs-lst-o))
;; (pict->png-n-page concentration-times-sqrs-o "Concentrations-squares-view-osprey") ; ok!

  
