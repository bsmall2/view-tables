#lang slideshow
(require  pict-abbrevs)
(require slideshow/text)
(require "../common/orgmode-transforms.rkt")
(require "../common/pict-procedures.rkt")
(require "../common/ps-draw-procedures.rkt") ;; for pict->page .ps file
(require "../common/qrcode-imgs.rkt")

(define BD-en "Bob Dylan")
(define BD-ja "ボブ・ディラン")
(match-define (list BD-en-p BD-ja-p)
  (map text (list BD-en BD-ja)))
(define BD-yng-p (scale-to-fit
                  (bitmap "imgs/head-w-harmonica.jpg") 350 350))
(define BD-intro-1-p (vl-append 5 BD-en-p BD-yng-p BD-ja-p))
;; (p-v-r BD-intro-1-p)

(define serve-en-ja-p
  (pair-poem-files"gotta-serve-somebody-en.txt" "gotta-serve-somebody-ja.txt"))

(define do-right-en-ja-p
  (pair-poem-files "do-right-en.txt" "do-right-ja.txt"))

(define serve-right-p (vl-append 30 serve-en-ja-p do-right-en-ja-p))
(define view-1 (pin-over serve-right-p serve-en-ja-p rb-find BD-intro-1-p)
  #;(vl-append 10 serve-en-ja-p BD-intro-1-p
                          do-right-en-ja-p ))

(pict->png-n-page view-1 "Serve-some-Do-Right")
