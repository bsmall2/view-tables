#lang scribble/base

@title{九州三大愚行}

@bold{共通項}いずれもムダな公共・公益事業で、 建設それ自体が目的となっている。

@;  'cont for 予算 in 円 
@tabular[#:sep @hspace[2]
	#:column-properties '(left left center center right)
	#:row-properties '(bottom-border ())
	(list
		(list @bold{案件} @bold{罪状} @bold{予算}  @bold{財源}  @bold{知名度})
		(list "長崎, 諫早湾干"
		@itemlist[#:style 'ordered 
			   @item{自然破壊(海洋)}]
		"2,500億円" "国税" "***")
		(list "熊本, 川辺川ダム"
		@itemlist[#:style 'ordered 
			   @item{自然破壊(清流)}
			   @item{集落水没}]
		@;"自然破壊(清流)、集落水没" 
		"2,500億円" "国税" "**")
		(list "宮崎, 子丸川揚水・送電線"
		@itemlist[#:style 'ordered 
			   @item{自然破壊(山と川)}
			   @item{景観破損}
			   @item{電磁波公害}]
		"3,500億円" "電気料金" "*"))]

@bold{人口}

@tabular[#:sep @hspace[2]
	 #:row-properties '(bottom-border ())
        (list (list "領域"  "人数")
	      (list "全国"  "1.2億人")
	      (list "九州"  "1,350万人"))]
		
     
         



