#+TITLE: データ三大愚行
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>


** 表

| 県   | 案件               | 総額(￥)        | 財源           | 知名度 | 罪数 |
|------+--------------------+-----------------+----------------+--------+------|
| 長崎 | 諫早湾干           | 250000000000 | 国税           |      3 |    1 |
| 熊本 | 川辺川ダム         | 250000000000 | 国税           |      2 |    2 |
| 宮崎 | 小丸川用水・送電線 | 350000000000 | 電気料金(九電) |      1 |    3 |

# (require (only-in scribble add-commas)) 
# 250,000,000,000
# 250,000,000,000
# 350,000,000,000


