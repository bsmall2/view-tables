True civilization is
not to despoil mountains
not to ruin rivers
not to destroy villages
not to kill people.
