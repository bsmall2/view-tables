#lang racket
(require (only-in net/url url-regexp))

(provide (all-defined-out))

(define (all-but-last lst)
  (take lst (sub1 (length lst))))

(define (is-url-link? str)
  (second (regexp-match url-regexp str)))

(define (num-str->num str)
  (string->number (string-replace str "," "")))

(define (M->number millions)
  (inexact->exact (* 1000000 millions)))
(define (Mstr->numb Ms)
  (M->number (string->number Ms)))

(define (num->Mils num)
  (exact->inexact (/ num 1000000)))
