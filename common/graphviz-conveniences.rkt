
;; #lang racket
;; (provide (all-defined-out))

(define digraph-link " -> ")
(define graph-link " -- ")

(define (->safe-graph-name str)
  (regexp-replace* #rx"-" str "_"))

(define graphviz-dot-start
  (list (string-append "digraph " (->safe-graph-name  in-base-str) " { ") " "
	;;"\n ranksep = 2"
	;; "\n size=\"10,10\""
	"\n node[shape = plaintext, fontsize = 16] "
	"\n edge[len=3]"
	"\n arrowsize=.5 \n"
	"\n len=2 "
	"\n\n"))
(define graphviz-dot-end (list " " "}"))
(define graphviz-dot-start-str (string-join graphviz-dot-start))
(define graphviz-dot-end-str (string-join graphviz-dot-end))

(define punctuation-rx-for-dot #rx"\\.|\\?|,")
(define (remove-punctuation-for-dot str) ;; string, or line
  (regexp-replace* punctuation-rx-for-dot  str  ""))

(define (set-gv-dot-notation-in-digraph nttn)
  (string-append graphviz-dot-start-str "\n\n" nttn "\n\n"
		 graphviz-dot-end-str))
		 
		 
