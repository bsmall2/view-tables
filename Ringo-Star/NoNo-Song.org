歌詞 Ringo Starr - No No Song

Huh-huh! Huh-huh!
(Ah-ah-ah-ah-ah)
(Aye-aye-aye-aye)

A lady that I know just came from Colombia
She smiled because I did not understand
Then she held out some marijuana, ha ha
She said it was the best in all the land

And I said
"No, no, no, no, I don't smoke it no more
I'm tired of waking up on the floor
No, thank you, please, it only makes me sneeze
And then it makes it hard to find the door"

(Ah-ah-aye-aye)

A woman that I know just came from Majorca, Spain
She smiled because I did not understand (Parazzi! Parazzi!)
Then she held out a ten pound bag of cocaine
She said it was the finest in the land

And I said
"No, no, no, no, I don't [sniff] no more
I'm tired of waking up on the floor
No, thank you, please, it only makes me sneeze
And then it makes it hard to find the door"

(Aye-aye-aye-aye)
(Aye-aye-aye-aye)
(Aye-aye-aye-aye)
(Aye-aye-aye)

A man I know just came from Nashville, Tennessee, oh
He smiled because I did not understand
Then he held out some moonshine whiskey, oh ho
He said it was the best in all the land (And he wasn't joking!)

And I said
"No, no, no, no, I don't drink it no more
I'm tired of waking up on the floor
No, thank you, please, it only makes me sneeze
And then it makes it hard to find the door"

Well, I said
"No, no, no, no, I can't take it no more
I'm tired of waking up on the floor
No, thank you, please, it only makes me sneeze
And then it makes it hard to find the door"
歌の翻訳 Ringo Starr - No No Song 日本語に
ふふふ！ふhっ！
（あああああああ）
（アイアイアイアイ）

私が知っている女性はコロンビアから来たばかりです
私は理解できなかったので彼女は微笑んだ
それから彼女はマリファナを差し出しました、ハハ
彼女はそれがすべての土地で最高だと言った

そして私は言った
「いや、いや、いや、いや、私はそれ以上喫煙しません
床で目覚めるのに疲れた
いいえ、ありがとうございます。くしゃみをするだけです
そして、それはドアを見つけるのを難しくします。」

（あああああああ）

私が知っている女性はマヨルカ、スペインから来たばかりです
私は理解できなかったので彼女は微笑んだ（パラッツィ！パラッツィ！）
それから彼女はコカインの10ポンドのバッグを差し出しました
彼女はそれがその土地で最高であると言いました

そして私は言った
「いいえ、いいえ、いいえ、いいえ、私はもう[sniff]しません
床で目覚めるのに疲れた
いいえ、ありがとうございます。くしゃみをするだけです
そして、それはドアを見つけるのを難しくします。」

（アイアイアイアイ）
（アイアイアイアイ）
（アイアイアイアイ）
（アイアイアイ）

私が知っている男はテネシー州ナッシュビルから来たばかりだよ
私は理解できなかったので彼は微笑んだ
その後、彼はムーンシャインウイスキーを差し出しました。 彼はそれがすべての土地で最高だと言った（そして彼は冗談ではなかった！）

そして私は言った
「いや、いや、いや、いや、もう飲まない
床で目覚めるのに疲れた
いいえ、ありがとうございます。くしゃみをするだけです
そして、それはドアを見つけるのを難しくします。」

まあ、私は言った
「いやいや、いや、いや、いや、私はそれをもうとることができません
床で目覚めるのに疲れた
いいえ、ありがとうございます。くしゃみをするだけです
そして、それはドアを見つけるのを難しくします」

 - https://flowlez.com/ja/songs/no-no-song-1315429/

 高校生のためにページ
  - https://blog.goo.ne.jp/ryuukoh/e/4ec81596c23e157a6045ae55bcd0c09b
