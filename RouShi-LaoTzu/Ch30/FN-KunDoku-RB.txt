https://aokikenji.com/tao-30
$道^タオ$を$以^も$って$人^じん主^しゅ$を$佐^たす$くる者は、
兵を以って天下に強いず。
その事は$還^かえ$るを好む。
師の処おる所は、
$荊^けい$棘^きょく$焉^ここ$に生じ、
大軍の後は、
必ず$凶^きょう$年^ねん$あり。
善くする者は果たして$已^や$む。
以って強いるを取らず。
果たして$矜^ほこ$ることなく、
果たして$伐^ほこ$ることなく、
果たして$驕^おご$ることなく、
果たして已むを得ずとす。
これを果たして強いるなしと$謂^い$う。
物は$壮^さかん$なればすなわち老ゆ。
これを不道と謂う。
不道は早く已む。
