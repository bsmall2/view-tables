#lang slideshow
(require  pict-abbrevs)
(require slideshow/text)
(require "../common/orgmode-transforms.rkt")
(require "../common/pict-procedures.rkt")

(provide (all-defined-out))

(define tanaka-ja-s "$田^た$中^なか$正^しょう$造^ぞう$")
(define t-j-p (kj-fg-mrkp-str->pct-v tanaka-ja-s 1 (kanji-style) 10))
(define tanaka-en-s "Shōzō Tanaka")
(define t-e-p (text tanaka-en-s))
(define TS1-pct (bitmap "imgs/Tanaka_Shozo.jpg"))
(define TS1-pct-head-only (bitmap "imgs/Tanaka_Shozo-head.jpg"))
(define TS1-all-180px-p (bitmap "imgs/180px-Shozo_Tanaka.jpg"))
;; (p-v-r TS1-all-180px-p) ; ok!
(define TS-SilentSitting-w-names-p (vc-append TS1-all-180px-p
                                              (vl-append
                                               (kj-fg-mrkp-str->pct tanaka-ja-s)
                                               t-e-p)))
;; (get-pct-dims TS-SilentSitting-w-names-p) ; ok!

(define TS1-full-w-names (ht-append (vl-append TS1-pct t-e-p)
                                    t-j-p))
;; (p-v-r TS1-full-w-names) ; ok! , for now very big!!


(define TS1-head-w-names (ht-append t-j-p (vl-append
                                           TS1-pct-head-only
                                           t-e-p)))
;; (p-v-r TS1-head-w-names) ; ok!
