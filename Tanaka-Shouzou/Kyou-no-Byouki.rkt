#lang slideshow
(require  pict-abbrevs)
(require slideshow/text)
(require "../common/orgmode-transforms.rkt")
(require "../common/pict-procedures.rkt")
(require "author-picts.rkt")
(require "book-picts.rkt")

(define ttl-s "今日の病気の数々")
(define ttl-furi-s "$今^きょ$日^う$の$病^びょう$気^き$の$数^かず$々^かず$")
(define ttl-furi-p (kj-fg-mrkp-str->pct-v ttl-furi-s))
;; (p-v-r ttl-furi-p)
(define good-lines-s (file->lines "Kyou-no-Byouki.txt"))
(define yamai-lines-p (bushi-lines->tate-pct good-lines-s 20 20))
(define owari-lines-s
  "その他病$百^ひゃく$出^しゅつ$せしハ今日の政治上の病気なり。
薬りでハだめ、
法律でハだめ、
ただ一ツ
精神療法一ツあるのみ。")
(define owari-lines-p
  (apply ht-append 20
         (map kj-fg-mrkp-str->pct-v
              (reverse (string->lines-bs owari-lines-s)))))
;; (p-v-r owari-lines-p) ;ok
(define base-img (ht-append 50
                            owari-lines-p
                            yamai-lines-p
                            ttl-furi-p))
;; (p-v-r base-img) ; ok, maybe

#;(p-v-r ;cuts of side of photo but usable
(pin-over base-img yamai-lines-p cc-find TS1-head-w-names)
)

(define-values (bg-w bg-h) (values 300 300))
(define auth-p (scale-to-fit (launder TS1-head-w-names) bg-w bg-h))
(define book-p (scale-to-fit (launder TSBS2-info-pct) bg-w bg-h))
(define-values (b-w b-h) (values (pict-width base-img) (pict-height base-img)))
(define base-w-auth
  (pin-over base-img owari-lines-p lb-find
            auth-p))

(p-v-r(pin-over base-w-auth auth-p lb-find
                book-p))
(pict->png-bg (pin-over base-w-auth auth-p lb-find
                        book-p)
              "Kyou-no-Byouki.png")


