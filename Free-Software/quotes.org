#+TITLE: Free Software Education Quotes
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>

* Free Software Education： 自由なソフト教育

** Why?：なぜ？ 

*** Key 鍵
#+begin_quote
“Schools should teach their students to be citizens of a strong, capable, independent and free society.”
#+end_quote

#+begin_quote
「学校は、強く、有能で自立した自由な社会の一員となるよう、学生を教育すべきです。」
#+end_quote

*** Sharing 
#+begin_quote
Schools should teach the value of sharing by setting an example. Free software supports education by allowing the sharing of knowledge and tools:
#+end_quote

#+begin_quote
学校は例示によって共有の価値を教えるべきです。自由ソフトウェアは知識とツールの共有を許容することにより教育を支援します:
#+end_quote

*** Social Responsibility： 社会的責任
#+begin_quote
Computing has become an essential part of everyday life. Digital technology is transforming society very quickly, and schools have an influence on the future of society. Their mission is to get students ready to participate in a free digital society by teaching them the skills to make it easy for them to take control of their own lives.
#+end_quote

#+begin_quote
コンピューティングは日常生活に不可欠なものとなりました。ディジタル技術が急速に社会を転換するなかで、学校もまた社会の未来に対して影響を持っています。学校の責務は、自身の暮らしをコントロールする技能を学生に教えることによって、学生が自由なディジタル社会へ参加できるようにすることにあります。
#+end_quote

*** Independence： 自立
#+begin_quote
Schools have an ethical responsibility to teach strength, not dependency on a single product or a specific powerful company. Furthermore, by choosing to use Free Software, the school itself gains independence from any commercial interests and it avoids vendor lock-in.
#+end_quote

#+begin_quote
学校には、強さを教えること、また特定の製品や特定の強大な企業への依存を教えない、といった倫理的責任があります。さらに、自由ソフトウェアの使用を選択することで、学校自身もいかなる商売の利益からも独立し、ベンダーによるロックインを避けることができるのです。
#+end_quote

*** Learning： 学習
#+begin_quote
When deciding where they will study, more and more students are considering whether a university teaches computer science and software development using Free Software. Free software means that students are free to study how the programs work and to learn how to adapt them for their own needs. Learning about Free Software also helps in studying software development ethics and professional practice.
#+end_quote

#+begin_quote
どこで学ぶかを決めるにあって、大学が自由ソフトウェアを使ってコンピュータ科学やソフトウェア開発を教えているかどうかを考慮する学生は増えてきています。自由なソフトウェアとは、プログラム動作の仕組みを学んだり必要に応じて修正の仕方を学んだりといったことを学生が自由に行えること、を意味します。自由ソフトウェアについて学ぶことはまたソフトウェア開発における倫理や実務を学習するのに役立ちます。
#+end_quote

*** Savings： 節約
#+begin_quote
This is an obvious advantage that will appeal immediately to many school administrators, but it is a marginal benefit. The main point of this aspect is that by being authorized to distribute copies of the programs at little or no cost, schools can actually aid families facing financial issues, thus promoting fairness and equal opportunities of learning among students.
#+end_quote

#+begin_quote
この点は、即座に多くの学校担当者の興味をそそる明らかな利点ですが、それほど重要ではない利点です。重要な点は、プログラムのコピーを少額ないしは無償で配布することが認められれば、経済的問題に直面している家庭を学校が実際に支援することができる、ということです。これにより、学生に対して公正で平等な学習の機会を推進することになるのです
#+end_quote

*** Quality： 品質
#+begin_quote
Stable, secure and easily installed Free Software solutions are available for education already. In any case, excellence of performance is a secondary benefit; the ultimate goal is freedom for computer users.
#+end_quote

#+begin_quote
安定で安全で簡単にインストールできる自由ソフトウェア・ソリューションはすでに教育向けに利用可能です。どんな場合でも、パフォーマンスの優位は二次的なものです。究極の目標は、コンピュータのユーザの自由なのです
#+end_quote
 - https://www.gnu.org/education/edu-why.html


*  Education： 教育

** Confucius Sharpen Tools For Good Works

#+begin_quote
Making the choice to use free software in the classroom helps us create learning environments that foreground questions of value. This can be illustrated by a passage in Confucius's Analects where the skilled mechanic is illustrated as sharpening their tools before they are able to do their work well. In the 21st century, and in the context of digital learning environments, to sharpen tools would mean to know not just how the tool works but what the tool means with respect to the place of the human in the world. This will be explored by considering the five Ws: what free software education is, who it is for, when and where it takes place, and why our active role as digital makers, not just passive users, is central to the meaning of free software education.
#+end_quote

   - https://www.fsf.org/bulletin/2022/spring/the-need-for-free-software-education-now
     - https://mstdn.jp/@bsmall2/112057229143284881
