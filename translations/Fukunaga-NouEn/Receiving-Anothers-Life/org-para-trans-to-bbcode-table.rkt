#lang racket

(define in-string (file->string "translation.org"))
;; in-string ; ok!

(define paras (cdr ;; how to allow for empty spaces only line??
	       (regexp-split  #px"\n\n+" in-string)))
;; (length paras)

(define (para->td txt)
  (string-append "[td]" txt "[/td]"))

(define tds (map para->td paras))

(define (td-txt->tr td-lst)
  (define (bb-row lst)
    (string-append "[tr]"
		   (first lst)
		   (second lst)
		   "[/tr]"))
  (define (helper lst keep)
    (cond
     ((<= 2 (length lst))
      (helper (drop lst 2) (cons (bb-row lst) keep)))
     ((= 1 (length lst))
      (reverse (cons (bb-row (append lst '("----"))) keep)))
     ((empty? lst) (reverse keep))))
  (helper td-lst '()))

(define trs (td-txt->tr tds))

(display-to-file 
 (string-append "[table border=0]"
		(apply string-append
		       (add-between trs "\n"))
		"[/table]")
 "translation-table.bb"
 #:exists 'replace)
  
