#+TITLE: Talk of Receiving the Life of Another
# Blessing of Another Life
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>


**  命をいただくお話し

** Talk of Receiving the Life of Another: Blessing from Another's Life

お米を育ててくれた合鴨を、最後には食べてしまう事を可哀想だといつも言われます。本当にその通りだと思います。人間も植物のように、自分で栄養素を作りだす事が出来たら他の生き物の命をいただく必要はないのにとつくづく思います。こんなにつらい気持ちを知らずに済むのにと思います。暑い夏中、良く働いてくれた合鴨達を送る日、私は彼らに感謝を捧げ、別れを告げた後、決まって頭から布団をかぶり枕に顔を押し付けて声を殺して泣き続けます。何百回もごめんなさいを繰り返します。声をかけると駆け寄ってきて、手から直接餌を食べるほど良く懐いてくれた彼らにもう会えないと思うと胸が潰れそうになります。最期の瞬間、苦しみませんように。そう祈っては涙が後から後から溢れて止まらなくなってしまうのです。

People feel sorry for the ducks because we eat them after they help us to grow rice. I think so to. I feel bad for the ducks too. I frequenlty think that it is a shame that humans cannot live like plants and make their own nutrients. We would be able to go on without these painful feelings of pity. In the heat of mid-summer, the day I see off the hard-working ducks, I thank them. After parting I burrow head-first under a blanket, push my face into a pillow, and smother the sounds of my continued crying. "I'm sorry" I cry hundreds of times. The ducks waddle to me when called, they eat directly from my hand. That is how used to humans, to me, they become. My chest feels crushed when I think I cannot meet them again. "That they feel no pain in their last moments", this I pray as my tears keep on, and keep on, flowing without pause.

そして、お肉となって帰ってきた合鴨を「おいしい、おいしい。ありがとう、ありがとう」と大切にいただきます。よく食べられるものだ、と言う人もいます。確かにその通りです。でも、私思うのです。合鴨のお肉をいただく事で、彼らが私の身体の一部となって私の命が尽きる日まで一緒に生きてくれるのだと。だから私は、これまで私の命を支えるために捧げて頂いたたくさんの命のためにも、私の命を輝かせ、自分だけでなく周囲の人も一緒に幸せに満ち溢れた人生を送ろうと誓いを立てたくなるのです。食べ物は、私の身体だけでなく人生も作ってくれているのだとしみじみ思います。誰かを誹謗中傷したりいじめたりする人がいますが、そんなことをしてもその人の命は輝きません。他を傷つけるような生き方を助けるために彼らは命を捧げるわけではないのです。そんな生き方は望まないのです。みんな仲良く幸せになってもらうために捧げられる命なのです。

  And then, when the ducks come back as meat, "Tasty, Tasty. Thank you, Thank you." I take great care while eating the blessings of their lives. "I can't believe you can eat them." is what some people say. I think so too. But I think something else too. Through receiving the blessings of their lives, they become part of my body and they go on living with me as long as there is life in me. Because of this, for all blessings of those lives that I have received to support my living, I make the most of my life. All these lives and their blessings make me want to swear that I will live a full life to make, not only myself, but also those around me live in a way full of happiness. Food, makes not only our bodies but our lives too is what I come to keenly feel. There are people who slander others, but that does not help them to live their fullest in a good way. They do not dedicate their lives to helping other live well by harming them. I do not hope to live that way. Life is to be given in a way that gets others to get along in happiness.

もうひとつ大切だと思うことがあります。いただく命に精一杯の敬愛を込めて、愛する家族のために美味しく料理することが礼儀であり償いではないかということです。ですから何一つ無駄にすることなどできません。一方で、食材という言い方がありますが、栄養素やカロリーと聞くと悲しくなってしまいます。食品添加物だらけの加工食品なら必要かもしれませんけど。合鴨肉は栄養面でも優れていますが、そういう評価だけでしたら一気に味気ないものになってしまいます。命の重みが栄養素やカロリーに置き換えられて違う次元のお話しになってしまうようで、何ともいたたまれなくなってしまうのです。

There is another thing that I think is important. Is it not true that it is simply manners and atonement toward the life, whose blessing we receive, that, for the love of family we should make delicious meals. For this reason I am not able to waste even a bit (of the duck meat). When people talk of "Ingredients", nutrients, and calories: they make me sad. When talking of additive-laden processed food products this sort of talk may be necessary, for all I know. To convert the value of life into terms of nutrients and calories is to move into a wrong way of speaking. It becomes unbearable for me.

コインに例えると、表は捧げて頂いた命への感謝、裏は自分が生きるための殺生で表裏一体と考える人がほとんどだと思います。これはある意味正しいと思います。でも私は、頂く命への感謝に対する裏面を殺生だけではなく、命を受け取る側の愛や祈りではないかと感じる時があります。感謝の裏は他を愛するという事ではないかと。頂いた命その尊さに精一杯真剣に向かい合って料理していると、彼らの私達への愛情がひしひしと伝わってきます。その愛情を受けて料理が私の愛情表現となり、家族の幸せを願う祈りとなって命をつないでいるのです。

With the parallel of a coin, on the one side we give thanks for the bounty of life which we receive, on the other side, we live by the destruction of other lives. Most people seem to think that these are two sides of the same thing. I think this is right in one sense. But at times, I feel that the other side of gratitude for the blessings of other lives, is not only destruction of life, but love and prayer on the part of the receiver. Is it not that wat is on the other side of gratitude is love for the other? While cooking I face the sacredness of the lives we recieve, and I feel their love toward us. To recieve that love and cook a meal is an expression of our love and a prayer for the happiness of our family, a prayer that unites all of our lives.

大自然の慈悲にすがらないと生きていけない私達です。幸せな人生を送る以外この恩に報いる道はないように思えるのです。

Without the mercy of nature we cannot go on living. I am able to think that to live a happy live there is no other way than to reciprocate our obligations to life in this manner.

これは合鴨達と暮らして感じた事です。

This is what I feel through living with ducks.

福永農園

Fukunaga Farm
