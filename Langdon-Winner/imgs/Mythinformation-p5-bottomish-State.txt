Other notable beneficiaries of the systematic use of vast
amounts of digitized information are public bureaucracies, in-
telligence agencies, and ever-expanding military organizations.
Ordinary people are, of course, strongly affected by the workings
of these organizations and by the rapid spread of new electronic
systems in banking, insurance, taxation, factory and office work,
home entertainment, and the like. They are also counted upon to
be eager buyers of hardware, software, and communications ser-
vices as computer products reach the consumer market.

But where in all of this is any motion toward increased
democratization and social equality or the dawn of a cultural
renaissance?
