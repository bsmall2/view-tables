Granted, such prominent trends could be altered. It is possible
that a society strongly rooted in computer and telecommunica-
tions systems could be one in which participatory democracy,
decentralized control, and social equality would be fully realized.
Progress of that kind would involve, however, concerted efforts
by society to remove the many difficult obstacles blocking those
ends. The writings of computer enthusiasts, however, seldom
propose such deliberate action. Instead, they strongly suggest
that the good society will be a natural spin-off from the fast pro-
liferation of computing devices. They evidently assume that there
is no need to try to shape the institutions of the information age
to maximize human freedom or to place limits upon concentra~
tions of power.

er ee es

 

pecan sian tL a ee sa
