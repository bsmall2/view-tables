magazines, electronic mail, computer teleconferencing, on-line
stock market and weather reports, computerized yellow pages,
shopping via home computer, and so forth. All such services are
supposed to add up to a cultural renaissance. In the words of
James Martin, writing in Telematic Society (Prentice-Hall, 1981):

“The electronic revolution will not do away with work, but it
does hold out some promises: most boring jobs can be done by
machines; lengthy commuting can be avoided; we can have
enough leisure to follow interesting pursuits outside our work;
environmental destruction can be avoided; the opportunities for
personal creativity will be unlimited.””
