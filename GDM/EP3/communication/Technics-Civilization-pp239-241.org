#+TITLE: Technics and Civilization: Paradox of Communication pp 239-241
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>

 https://mstdn.jp/@bsmall2/111962318508704673

** p 239

7: The Paradox of Communication

Communication between human beings begins with the immediate
physiological expressions of personal contact, from the howlings and
cooings and head-turnings of the infant to the more abstract gestures
and signs and sounds out of which language, in its fulness, develops.
With hieroglyphies, painting, drawing, the written alphabet, there
grew up during the historic period a series of abstract forms of
expression which deepened and made more reflective and pregnant
the intercourse of men. The lapse of time between expression and
reception had something of the effect that the arrest of action pro-
duced in making thought itself possible.

With the invention of the telegraph a series of inventions began
to bridge the gap in time between communication and response de-
spite the handicaps of space: first the telegraph, then the telephone,
then the wireless telegraph, then the wireless telephone, and finally
television, As a result, communication is now on the point of return-
ing, with the aid of mechanical devices, to that instantaneous reaction
of person to person with which it began; but the possibilities of this
immediate meeting, instead of being limited by space and time, will
be limited only by the amount of energy available and the mechanical
perfection and accessibility of the apparatus. When the radio tele-
phone is supplemented by television communication will differ from
direct intercourse only to the extent that immediate physical con-

** p 240

240 TECHNICS AND CIVILIZATION

tact will be impossible: the hand of sympathy will not actually grasp
the recipient’s hand, nor the raised fist fall upon the provoking head.
‘What will be the outcome? Obviously, a widened range of inter-
course: more numerous contacts: more numerous demands on atten-
tion and time, But unfortunately, the possibility of this type of
immediate intercourse on a worldwide basis does not necessarily
mean a less trivial or a less parochial personality. For over against
the convenience of instantaneous communication is the fact that the
great economical abstractions of writing, reading, and drawing, the
media of reflective thought and deliberate action, will be weakened.
‘Men often tend to be more socialized at a distance, than they are in
their immediate, limited, and local selves: their intercourse some-
times proceeds best, like barter among savage peoples, when neither
group is visible to the other. That the breadth and too-irequent repeti-
tion of personal intercourse may be socially ineflicient is already
plain through the abuse of the telephone: a dozen five minute con-
versations can frequently be reduced in essentials to a dozen notes
whose reading, writing, and answering takes less time and effort
and nervous energy than the more personal calls. With the telephone
the flow of interest and attention, instead of being self-directed, is at
the mercy of any strange person who secks to divert it to his own
purposes.
One is faced here with a magnified form of a danger common to
all inventions: a tendency to use them whether or not the occasion
demands. Thus our forefathers used iron sheets for the fronts of
buildings, despite the fact that iron is a notorious conductor of heat:
thus people gave up learning the violin, the guitar, and the piano
when the phonograph was introduced, despite the fact that the passive
listening to records is not in the slightest degree the equivalent of
active performance; thus the introduction of anesthetics increased
fatalities from superfluous operations. The lifting of restrictions upon
close human intercourse has been, in its first stages, as dangerous as
the flow of populations into new lands: it has increased the areas
of friction, Similarly, it has mobilized and hastened mass-reactions,
like those which occur on the eve of a war, and it has increased the
dangers of international conflict. To ignore these facts would be to

** p241

   THE NEOTECHNIC PHASE  241

paint a very falsely over-optimistic picture of the present economy.

Nevertheless, instantaneous personal communication over long
distances is one of the outstanding marks of the neotechnic phase:
it is the mechanical symbol of those world-wide cooperations of
thought and feeling which must emerge, finally, if our whole civiliza-
tion is not to sink into ruin. The new avenues of communication have
the characteristic features and advantages of the new technics; for
they imply, among other things, the use of mechanical apparatus to
duplicate and further organic operations: in the long run, they
promise not to displace the human being but to re-focus him and
enlarge his capacities. But there is a proviso attached to this promise:
namely, that the culture of the personality shall parallel in refine-
ment the mechanical development of the machine. Perhaps the great-
est social effect of radio-communication, so far, has been a political
one: the restoration of direct contact between the leader and the
group. Plato defined the limits of the size of a city as the number
of people who could hear the voice of a single orator: today those
limits do not define a city but a civilization, Wherever neotechnic
instruments exist and a common language is used there are now the
elements of almost as close a political unity as that which once was
possible in the tiniest cities of Attica. The possibilities for good and
evil here are immense: the secondary personal contact with voice and
image may increase the amount of mass regimentation, all the more
because the opportunity for individual members reacting directly
upon the leader himself, as in a local meeting, becomes farther and
farther removed. At the present moment, as with so many other neo-
technic benefits, the dangers of the radio and the talking picture seem
greater than the benefits. As with all instruments of multiplication the
critical question is as to the function and quality of the object one is
multiplying. There is no satisfactory answer to this on the basis of
technics alone: certainly nothing to indicate, as the earlier exponents
of instantaneous communication seem uniformly to have
thought, that the results will automatically be favorable to the com-
munity.

** p243 Permantent Record, selections follow 
   
One may perhaps over-rate the changes in human behavior that
followed the invention of these new devices; but one or two suggest
themselves. Whereas in the eotechnic phase one conversed with the
mirror and produced the biographical portrait and the introspective
biography, in the neotechnic phase one poses for the camera, or
still more, one acts for the motion picture. The change is from an
introspective to a behaviorist psychology, from the fulsome sorrows
of Werther to the impassive public mask of an Ernest Hemingway.
Facing hunger and death in the midst of a wilderness, a stranded
aviator writes in his notes: “I built another raft, and this time took
off my clothes to try it. I must have looked good, carrying the big
logs on my back in my underwear.” Alone, he still thinks of himself
asa public character, being watched: and to a greater or less degree
everyone, from the crone in a remote hamlet to the political dictator
on his carefully prepared stage is in the same position. This constant
sense of a public world would seem in part, at least, to be the result
of the camera and the camera-eye that developed with it. If the eye
be absent in reality, one improvises it wryly with a fragment of
one’s consciousness, The change is significant: not self-examination   

** p244 camera exposure

but self-exposure: not tortured confession but easy open candor: not
the proud soul wrapped in his cloak, pacing the lonely beach at mid-
night, but the matter-of-fact soul, naked, exposed to the sun on the
beach at noonday, one of a crowd of naked people. Such reactions are,
of course, outside the realm of proof; and even if the influence of the
camera were directly demonstrable, there is little reason to think
that it is final. Need I stress again that nothing produced by technics
is more final than the human needs and interests themselves that have
created technics?   

** p245 para 2

At first these new recording and reproducing devices have con-
fused the mind and defied selective use: no one can pretend that we
have yet employed them, in any sufficient degree, with wisdom or
even with ordered efficiency. But they suggest a new relationship
between deed and record, between the movement of life and its col-
lective enregistration: above all, they demand a nicer sensitiveness
and a higher intelligence. If these inventions have so far made
monkeys of us, it is because we are still monkeys.   

