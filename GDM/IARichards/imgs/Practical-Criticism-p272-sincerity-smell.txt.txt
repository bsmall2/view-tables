272 PRACTICAL CRITICISM

On the ways in which sincerity may be increased and
extended Confucius is very definite. If we seek a stand-
ard for a new response whose sincerity may be in doubt,
we shall find it, he says, in the very responses which make
the new one possible. The pattern for the new axe-handle
is already in our hand, though its very nearness, our firm
possession of it, may hide it from us. We need, of course,
a founded assurance of the sincerity of these instrumental
responses themselves, and this we can gain by compari-
son. What is meant by “making the thoughts sincere”
is the allowing no self-deception “as when we hate a bad
smell, and as when we love what is beautiful” (The
Great Learning, VI, i). When we hate a bad smell we
can have no doubt that our response is sincere. We can
all, at least, find some responses beyond suspicion. ‘These
are our standard. By studying our sincerity in the fields
in which we are fully competent we can extend it into
the fields in which our ability is still feeling its way. This
seems to be the meaning of “choosing what is good and
firmly holding fast to it,” where “good” stands not for
our Western ethical notion so much as for the fit and
proper, sane and healthy. The man who does not “hate
a bad smell” “does not understand what is good”; hav-
ing no basis or standards, “he will not attain to sincerity.”

Together with these, the simplest most definite re-
sponses, there may be suggested also, as standards for
sincerity, the responses we make to the most baffling ob-
jects that can be presented to our consciousness. Some-
thing like a technique or ritual for heightening sincerity
might well be worked out. When our response to a poem
after our best efforts remains uncertain, when we are
