
教訓

土呂久鉱害事件は、 恵まれた自然と共生していた山奥に人々が、 資源をすいあげ毒物を放棄する鉱山操業の犠牲にされ、 命と健康を侵された惨劇だった。

現在も世界各地で、「富は中央に犠牲は辺地に」 という文明に傷めつけられている例が多くみられる。 山村のダム建設、 魚村の原子力発電所建設、 途上国の熱帯林伐採や鉱山開発......。 土呂久の悲劇を繰り返さないためにも、 現代社会の足もとを見つめ直し、 辺地や途上国の犠牲の上に成り立つ豊さを問い直す必要があるだろう。
