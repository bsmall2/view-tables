#+TITLE: 石牟礼道子 MINAMATA  現代の荒野から
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>


** 現代の荒野から
   
*** テキスト
#+begin_verse
現代社会の中に水俣を浮上させてみると、
いまだ読み解かれぬ仏典や、聖書の地にいる気がする。
人の罪あるいは神の罪がここの甦り、わたしたちは
より重い贖罪の時代にはいった。
そのような意味で、ここは選ばれた聖地であり荒野である。
仏典や聖書は現代においては生ま身の苦しみを描いた
倫理の規範だけれども、
生きている聖地には毒と血がながされる。
現世に居場所を失くした異形の人々に神の面影が宿るのは、
その心と軀から流される血のゆえである。
このような世界に生きていることは真実に恐しい。
ここには神が造り給うたものがなんでもある。
神話的次元での残酷と野蛮と偽善と策謀、絶望の底の
愛と戦いがあり、神もくつろぐ野のエロスまである。
そして人は、おのが中身に見合った外貌をつけて
秘蹟の場所にあらわれる。
神の讃えと罰とが峻別されて、
そのとき、 人々の表情にあらわれる。
ユージン・スミスとアイリーンは、
そのような表惜の瞬間を人間的肉眼でとらえ、
現代の聖画を再構成して見せるのである。
#+end_verse


*** 検索
    - 贖罪 しょくざい
      - https://kanjitisiki.com/jis2/2-3/721.html
#+begin_quote
音読み「ショク」
訓読み「あがなう」「あがない」

あがなう。物と物を交換する。金品で罪を免れる。
#+end_quote
   - 軀
     - https://kanjijoho.com/kanji/kanji17651.html
#+begin_quote
ク
からだ
むくろ
#+end_quote
    - 蹟
      - https://kanjijoho.com/kanji/kanji2564.html
#+begin_quote
音読み 	
    シャク
    セキ
訓読み 	
    あと
#+end_quote
  - 峻
    - https://kanjitisiki.com/zinmei/116.html
      
