#lang slideshow
(require  pict-abbrevs)
(require slideshow/text)
(require "../../common/orgmode-transforms.rkt")
(require "../../common/pict-procedures.rkt")
(require "../../common/ps-draw-procedures.rkt")

;; "花を奉る"
(define poem-title-s "$花^はな$ を $奉^たてまつ$る")
;;(define poem-title-p (p-it-kj poem-title-s))
;; (p-v-r poem-title-p)  ; ok!
(define poem-title-p (kj-fg-mrkp-str->pct-v poem-title-s
                                            (kanji-furi-gap) (cons 'italic (kanji-style))))
;; (p-v-r poem-title-p)   ; ok!
;; "石牟礼道子"
(define ishimure-michiko-ja-s "$石^いし$牟^む$礼^れ$道^みち$子^こ$")
(define IM-ja-p (kj-fg-mrkp-str->pct-v ishimure-michiko-ja-s 1 (kanji-style)));; 10
;; (p-v-r IM-ja-p) ; ok! ; but the size arg isn't being carried through always 36(kanji-size)
(define ishimure-en-s "Michiko Ishimure")
(define IM-e-p (text ishimure-en-s null (/ (kanji-size) 2))) ;;; adjust size later...
;; (p-v-r (hc-append IM-ja-p IM-e-p)) ; ok! but

(define bmp-pct-head (bitmap "../imgs/Michiko_Ishimure-head.jpg")) ; bmp-pct-head
(define bmp-pct-head-n-hands (bitmap "../imgs/Michiko_Ishimure-head-n-hands.jpg")) ; bmp-pct-head-n-hands
(define phto-n-en-hnds (vl-append 10 bmp-pct-head-n-hands IM-e-p))
;; (pict->png-bg phto-n-en "Ishimure-san-1967-head-n-hands-en.png")
(define intro-photo-hnds-names-title (ht-append 10 IM-ja-p phto-n-en-hnds poem-title-p)) 
;;(pict->png-bg intro-photo-hnds-names-title "Ishimure-san-1967-head-n-hands-HanaWoTatematsuru.png") ; ok!
(define phto-n-en-shrt (vl-append 10 bmp-pct-head IM-e-p))
(define intro-photo-shrt-names-title (ht-append 10 IM-ja-p phto-n-en-shrt poem-title-p))
;;(pict->png-bg  intro-photo-shrt-names-title "Ishimure-san-1967-head-HanaWoTatematsuru.png")

(define terms-file "Hana-wo-Tatematsuru-terms.txt")
(define term-lines (file->lines terms-file))

(define good-lines-strs (file->lines "Hana-wo-Tatematsuru-furi.txt"))
(define furi-lines-pct
  (apply ht-append 10
         (pict-bbox-lt
                (map kj-fg-mrkp-str->pct-v
                     (reverse good-lines-strs)))))
;; (pict->png-bg furi-lines-pct "Hana-wo-Tatematsuru-lines.png") ; ok!
#;(pict->png-bg (vl-append 20 furi-lines-pct intro-photo-shrt-names-title)
                         "HanaWoTatematsuru-furi-lines-w-intro-img.png")

;;;;;; Moved utilities to common org-transforms and pict-procedures 
#;(pict->png-bg (org-defns-str->term-defs-lst-v (file->string terms-file))
                "terms-definitions-plain-nofuri.png")
(define term-defns-p (org-defns-str->term-defs-lst-v (file->string terms-file)))

(define bg-info-p (ht-append 60 intro-photo-shrt-names-title term-defns-p))
;; (p-v-r bg-info-p)  ; ok! remove bold later hard to read?? change style?
(define page-pct (vl-append 80 furi-lines-pct bg-info-p))
;; (p-v-r page-pct) ;; ok!
;; (pict->png-bg page-pct "HanaWoTatematsuru-page-w-photo-defns.png")
;; (pict->page page-pct "HanaWoTatemasturu-page-w-photo-defns-lndscp.ps")
(define-values (p-w p-h) (values (pict-width page-pct)(pict-height page-pct)))
(define book-str-1 "「水俣へ」")
(define book-str-2 "受け継いで語る")
(define book-str-3 "水俣フォーラム編")
(define book-info-pct (vl-append (text "In:")
                   (text book-str-1 null (kanji-size))
                   (text book-str-2 null (/ (kanji-size) 2))
                   (text book-str-3 null (/ (kanji-size) 2))
                   (text "岩波書店" null (/ (kanji-size) 2))))
(define-values (b-i-w b-i-h) (values (pict-width book-info-pct) (pict-height book-info-pct))) ;; (list p-w p-h b-i-w b-i-h)
;; '(1862.0 1679.0 180.0 144.287109375)
(define-values (b-i-x b-i-y) (values (- p-w b-i-w) (- p-h b-i-h)))

(define page-pct-w-bk-fo
  (pin-over page-pct
            (- b-i-x 30)
            (- b-i-y 30)
            book-info-pct))
(pict->png-bg  page-pct-w-bk-fo "Hana-wo-Tatematsuru-w-book-info.png")

