#lang racket
(require pict pict-abbrevs)
(require "../common/pict-procedures.rkt")
(require "../common/orgmode-transforms.rkt")
(require "../common/kanji-furig-markup.rkt")

(nme-txt-lst->pct (list "title"  ;; (pan-spot-view title) ; ok!
			"鉱^こう$毒^どく$被^ひ$害^がい$発^はっ$生^せい$のメカニズム"))
(nme-txt-lst->pct (list "mine" "$採^さい$鉱^こう"))
(nme-txt-lst->pct (list "mn-desc" "鉱^こう$石^せき$を$掘^ほ$る")) ;; (pan-spot-view mn-desc) ; ok!
(nme-txt-lst->pct (list "tailings" "廃^はい$石^せき"))
(nme-txt-lst->pct (list "slag" "鉱^こう$滓^さい"))
(nme-txt-lst->pct (list "slag-desc" "焼^や$きがら"))
(nme-txt-lst->pct (list "pollutants" "鉛^なまり$、$亜^あ$ 砒^ひ$ 、$銅^どう$ 、カドミウム、$砒^ひ$素^そ$、$鉄^てつ$"))
;; (pan-spot-view pollutants) ; ok!

;; for accurate arrow placement to description, might need to avoid scaling?? 
(define src-dsc-rto 4/5)
(define (->dcs-sze src)
  (define-values (w h) (values (pict-width src)(pict-height src)))
  (define-values (w-d h-d)
    (values (* src-dsc-rto w) (* src-dsc-rto h)))
  (launder 
   (scale-to-fit src w h-d)))
(define pollutants-as-desc (->dcs-sze pollutants))
(pan-spot-view pollutants-as-desc)  
(define (make-src-dsc-pct src dsc) ;; pict pict -> pict
  (define-values (w h) (values (pict-width src)(pict-height src)))
  (define-values (w-d h-d)
    (values (* src-dsc-rto w) (* src-dsc-rto h)))
  (vc-append src (scale-to-fit dsc w h-d)))
(define slag-src-desc (make-src-dsc-pct slag slag-desc))
(pan-spot-view slag-src-desc) ; ok!

(define (make-shared-desc p1 p2 desc)
  (define w-d (pict-width desc))
  (define w-ps (pict-width (hbl-append 40 p1 p2))) ;; just in case, define gap later
  (define w-b (argmax values (list w-d w-ps)))
  (define h-d (pict-height desc))
  (define h-p (pict-height (argmax pict-height (list p1 p2))))
  (define all-h (+ h-p h-d))
  (define blnk (blank w-b all-h))
  (let* ((p (lt-superimpose blnk p1))
         (p (rt-superimpose p p2))
         (p (cb-superimpose p desc)))
    p))

;; OK! just make
;; (define make-source-desc-pict src desc) ;; with strings
;; (vc-append slag slag-desc) ;; slag-desc should be smaller
(define mine-pollution-combo
  (make-shared-desc (vc-append slag slag-desc)  tailings pollutants))

;; (add-rectangle-background #:radius 0  mine-pollution-combo)

