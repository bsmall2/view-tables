https://kln.or.kr/lines/poetryView.do?bbsIdx=1038

Wishing not to have so much as a speck of shame
toward heaven until the day I die,
I suffered,
even when the wind stirred the leaves.
With my heart singing to the stars,
I shall love all things that are dying.
And I must walk the road
that has been given to me.
 
Tonight, again, the stars are brushed by the wind.
