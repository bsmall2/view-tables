#lang slideshow
(require  pict-abbrevs)
(require slideshow/text)
(require "../common/orgmode-transforms.rkt")
(require "../common/pict-procedures.rkt")
(require "../common/ps-draw-procedures.rkt") ;; for pict->page .ps file
(require "../common/qrcode-imgs.rkt")

(define stdn-bmp (bitmap "imgs/head-student.jpg"))
;; (p-v-r (hc-append 20 stdn-bmp grad-bmp)) ; ok! grad-bmp huge!!
(define grad-bmp-big (bitmap "imgs/head-grad.jpg"))
(define bmp-scl (pict-height stdn-bmp))
(define grad-bmp-scl (scale-to-fit grad-bmp-big bmp-scl bmp-scl #:mode 'preserve))
;; (p-v-r grad-bmp-scl)  ok!
(define Yun-Ko "윤동주")
(define Yun-Ja "尹東柱")
(define Yun-furi "$尹^ユン$東^ドン$柱^ジュ$")
(define Yun-En "Yun Dongju")
(define Yun-ja-v (kj-fg-mrkp-str->pct-v Yun-furi))
(define Yun-ko-p (kj-fg-mrkp-str->pct Yun-Ko))
(define Yun-ko-v  (kj-fg-mrkp-str->pct-v Yun-Ko))
(define Yun-en-p (text Yun-En null 20))
(define Yun-pct-1
 (ht-append 5 Yun-ko-v
            (vl-append stdn-bmp Yun-en-p)
            Yun-ja-v))
(define Yun-pct-2
  (ht-append 5 Yun-ko-v
             (vl-append grad-bmp-scl Yun-en-p)
             Yun-ja-v)) ;; (p-v-r Yun-pct-2) ; ok!
(define ttl-Ko "서시")
(define-values (ttl-Ja ttl-en-1 ttl-en-2)  (values "序詩" "Prelude" "Foreword"))
(match-define (list t-k-p t-j-p t-e-1-p t-e-2-p)
  (map (λ (t) (text t null 18))
       (list ttl-Ko ttl-Ja ttl-en-1 ttl-en-2)))
(define ttls-sep-p (text "、" null 16))
(define titles-p (apply hbl-append 5
                        (add-between (list t-k-p t-j-p t-e-1-p t-e-2-p) ttls-sep-p)))
(define Yun-pre-pct
  (vc-append 10 titles-p Yun-pct-1)) ; (p-v-r Yun-pre-pct)  ; ok!
;;  AP: allpoetry.com;  KLN: Korean Literature Now; BS: Brian's translation
;; ; WS wikisource
(define-values (pre-ko-f pre-ja-f pre-en-BS-f pre-en-KLN-f pre-en-AP-f pre-ja-WS-f)
  (values "Ko-JoShi.txt" "Ja-JoShi.txt" "En-Prelude.txt" "En-lit-lined.txt" "En-JoShi-lined.txt" "Ja-SoShi-wikisource.txt"))
(define-values (AH-ko-w-knji-f  AH-ko-lnd-f AH-ko-horiz-f AH-ja-1-f AH-ja-2-f AH-en-1-f AH-en-2-f AH-en-BS-f)
  (values "skeleton-ko-AnotherHome-knji.txt"
   "skeleton-ko-AnotherHome-lined.txt"
   "skeleton-ja-AnotherHome-2-horiz.txt"
   "skeleton-ja-AnotherHome.txt"
   "skeleton-ja-AnotherHome-2.txt"
   "skeleton-en-Cook-lined.txt"
   "skeleton-en-AP-lined.txt"
   "skeleton-en-BS.txt"))
;; (file->lines pre-ja-WS-f) ; ok!
(define-values (KLN-lnk AP-lnk WS-lnk blg-lnk)
  (values "https://kln.or.kr/lines/poetryView.do?bbsIdx=1038"
          "https://allpoetry.com/Dongju-Yun"
          ;; https://allpoetry.com/poem/8611597-Prelude-by-Dongju-Yun
          "https://ja.wikisource.org/wiki/%E5%BA%8F%E8%A9%A9"
          "https://ameblo.jp/kjroad/entry-12187190412.html"))
(match-define (list KLN-qr AP-qr WS-qr blg-qr)
  (map  qr->pict (list KLN-lnk AP-lnk WS-lnk blg-lnk)
        (list "KLN-link" "AP-lnk" "wikisource-link" "ameblo-blog-link")))
(qr-pict-maker code-qr "code-link" "https://codeberg.org/bsmall2/view-tables/src/branch/main/Yun-Dong-ju") ;; (p-v-r code-qr) ; ok! 
(match-define (list KLN-p AP-p WS-p blg-p)
	      (map qr-w-intro
		   (list KLN-qr AP-qr WS-qr blg-qr)
		   (list "English trans. 2" "English trans. 3" "朝鮮語と日本語" "ブログ解説")))
(define code-p (qr-w-intro code-qr "Code Source プログラム"))
(define qrs-p (vc-append 5
                         (text "English trans. 1 by Brian")
                         code-p
                         (ht-append 30
                                    (vl-append 5 KLN-p AP-p)
                                    (vl-append 5 WS-p blg-p))))
(define (get-Yun-lines fn (intro-lns 2))
  (drop (file->lines fn) intro-lns))
(define (f-lines->tate-p fn (skp 2))
  (kj-fg-lines->nmbrd-pct-v (get-Yun-lines fn skp)))
(define (f-lines->yoko-p fn (skp 2))
  (kj-fg-lns->nmbrd-pct (get-Yun-lines fn skp)))
(match-define (list en-BS-p en-KLN-p en-AP-p)
  (map f-lines->yoko-p (list pre-en-BS-f pre-en-KLN-f pre-en-AP-f)))
(match-define (list AH-ko-h AH-ko-Cook AH-ja-2 AH-en-1 AH-en-2 AH-en-BS)
  (map (λ (f) (f-lines->yoko-p f 0)) (list AH-ko-lnd-f AH-ko-horiz-f AH-ja-2-f AH-en-1-f  AH-en-2-f AH-en-BS-f)))
(define En-vers
(ht-append 50 en-BS-p en-KLN-p en-AP-p))
(define En-vers-v
  (vl-append 50 en-BS-p en-KLN-p en-AP-p))
(define AH-en-vers
  (vl-append 50 AH-en-1 AH-en-2))

(match-define (list Ko-v-p Ja-v-p Ja-v-p-2)
  (map f-lines->tate-p (list pre-ko-f pre-ja-f pre-ja-WS-f)))
(match-define (list AH-ko-w-knji AH-ko-v AH-ja-v)
  (map (λ (f) (f-lines->tate-p f 0)) (list AH-ko-w-knji-f  AH-ko-lnd-f AH-ja-1-f)))
(define Ko-Ja-vers-v (ht-append 50 Ko-v-p Ja-v-p))
(define Ko-Ja-vers-v-face-name (ht-append 20 Ko-v-p Yun-pre-pct Ja-v-p-2))

;; AH-ko-v  make vertical Korean format match orig, like in Iwanami book
;; ;;                                                             ; with Kanji-Hangul Mix... 
(define AH-ko-ja-v-W-face (ht-append 20 AH-ko-w-knji  Yun-pct-2 AH-ja-v)) ; was Yun-pct-1 
(define AH-vs-w-face  AH-ko-ja-v-W-face)
;; (pict->png-n-page Ko-Ja-vers-face-name "Ko-Ja-vers-face-name") ; ok!
(match-define (list Ko-h-p Ja-h-p)
  (map f-lines->yoko-p (list pre-ko-f pre-ja-f)))
;;(p-v-r (ht-append 50 Ko-h-p Ja-h-p)) ;; Korean takes up more space
(define Ko-Ja-p (pair-poem-lines
                 (get-Yun-lines pre-ko-f)
                 (get-Yun-lines pre-ja-f)))
(define AH-ko-ja-p (pair-poem-lines
                    (get-Yun-lines AH-ko-lnd-f 0)
                    (get-Yun-lines AH-ja-2-f 0))) 
(define AH-enCook-jaKim (pair-poem-lines
                         (get-Yun-lines  AH-en-1-f  0)
                         (get-Yun-lines AH-ko-horiz-f 0)))
;; (p-v-r AH-enCook-jaKim) ; ok! ; but how to adjust gap? 
(define Ko-Ja-hrz-vrt-auth (vc-append 50 Ko-Ja-p Ko-Ja-vers-v-face-name))
;; (pict->png-n-page Ko-Ja-hrz-vrt-auth "Ko-Ja-hrz-vrt-auth")
(define Ko-Jas-ttl-pct-Ens
  (vc-append Ko-Ja-hrz-vrt-auth En-vers))
;; (pict->png-n-page Ko-Jas-ttl-pct-Ens "Ko-Jas-ttl-pct-Ens")
(define Ko-Jas-ttl-pct-Ens-v
  (ht-append 20 En-vers-v Ko-Ja-hrz-vrt-auth))
;; (pict->png-n-page Ko-Jas-ttl-pct-Ens-v "Ko-Jas-ttl-pct-Ens-v")
(define Ko-Jas-Ens-v-under
  (vc-append 20 Ko-Ja-hrz-vrt-auth En-vers-v))
(define vers-w-qrs
  (pin-over Ko-Jas-ttl-pct-Ens-v Yun-pre-pct cb-find
            (inset qrs-p 0  40)))
;;(pict->png-n-page vers-w-qrs "Yun-Prelude-vers-q-qrs")
(define vers-w-qrs-lngr
  (pin-over Ko-Jas-Ens-v-under Yun-pre-pct lb-find
            (inset qrs-p 50 25)))
;; (pict->png-n-page vers-w-qrs-lngr "Yun-Prelude-vers-q-qrs-lngr")
(define AH-view-1 (vc-append 50 AH-ko-ja-p AH-vs-w-face  AH-en-vers))
;; (pict->png-n-page AH-view-1 "Another-Homeland-view-1")
(define AH-view-2 (vc-append 150
                             AH-ko-ja-p
                             AH-vs-w-face
                             AH-enCook-jaKim
                             (ht-append 90 AH-en-2 AH-en-BS)
                             ))
;; (pict->png-n-page AH-view-2 "Another-Homeland-view-2") ; ok!
;; add AHL qrs
(define qr-sze 80)
(qr-pict-maker-sizer AH-allpoetry-p "AH-allpoetry-site"
                     "https://allpoetry.com/Yet-Another-Home"
                     "allpoetry site" qr-sze)
(qr-pict-maker-sizer AH-Cook-p "Cook-Chung-Hyo-pdf"
                     "https://s-space.snu.ac.kr/bitstream/10371/2661/3/englishstudies_v02_057.pdf"
                     "essay pdf" qr-sze)
(qr-pict-maker-sizer AH-ja-blg-p "AH-goo-blog-ja-trans"
                     "https://blog.goo.ne.jp/okuyeo/e/2404651a628cbab5f86fc7d00b7a766f"
                     "日本語訳" qr-sze)
(qr-pict-maker-sizer Ja-iwnmi-site-p "Iwanami-book-site"
                     "https://www.iwanami.co.jp/book/b248425.html"
                     "日本の本" qr-sze)
(qr-pict-maker-sizer Ja-wiki-p "Yun-wikipedia-page"
                     "https://ja.wikipedia.org/wiki/%E5%B0%B9%E6%9D%B1%E6%9F%B1"
                     "日本wikip" qr-sze)
(qr-pict-maker-sizer ko-w-ja-p "Ko-w-Ja-amblo-blg"
                     "https://ameblo.jp/shimogashiwa/entry-12366504511.html"
                     "漢字とハングル" qr-sze)
(define qrs-lst (list Ja-iwnmi-site-p AH-ja-blg-p ko-w-ja-p  AH-allpoetry-p AH-Cook-p Ja-wiki-p))
(define qrs-vert (apply vl-append 10 qrs-lst)) ;; (p-v-r qrs-vert) ;ok!
(define AH-v2-w-qrs
  (pin-over AH-view-2 Yun-pct-2 cb-find qrs-vert))
(pict->png-n-page AH-v2-w-qrs "AH-view-2-w-qrs")

(define AH-h-v-w-face
  (pin-to-lb-crnr AH-ko-ja-p Yun-pct-2))
;; (pict->png-n-page AH-h-v-w-face "Another-Homeland-ko-ja-grad-pct")
