Prelude (Dongju Yun)

Let me have no shame
Under the heaven
Till I die.
Even winds among the foliage
Pained my heart.

With a heart that sings of the stars,
I'll love all dying things.
And I must fare the path
That's been allotted to me.

Tonight also
The winds sweep over the stars.
