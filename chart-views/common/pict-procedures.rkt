#lang racket
(require pict pict-abbrevs)
(require racket/syntax syntax/parse/define)
(require (for-syntax racket/syntax))

(require "orgmode-transforms.rkt")
(require "kanji-furig-markup.rkt")

(provide (all-defined-out))
;; preview large picts in DrRacket or emacs racket-mode
;;; preview in emacs' racket-mode when buffer has dark background
(define (pan-spot-view pct (x-m 80) (y-m 50)
                       (clr "pale green")(brdr-clr "forest-green")(btm-mrgn 20))
  (panorama
   (add-spotlight-background #:x-margin x-m #:y-margin y-m
                             #:color clr #:border-color brdr-clr
                             (inset pct 5 5 5 btm-mrgn))))
(define (p-v pct) (pan-spot-view (scale-to-fit pct 640 480)))

;;;; string utilities ;;;;;
(define (blank-string-bs? str) ;; Japanese spaces in [:space:] ? 
  (or (= 0 (string-length str))
      (regexp-match #px"^[[:space:]].*$" str)))

(define (empty-string-bs? str)
  (not (non-empty-string? str)))

;;; pict utilities ;;;;;
(define (add-bg pct (clr "white"))
  (add-rectangle-background pct #:radius 0 #:color clr))

(define (add-mrgns pct (l 8)(t 6)(r 8)(b 12)) ;; pad pict like a page
  ;; https://www.lifewire.com/create-perfectly-proportioned-page-margins-1074472
  ;; ; Make the top margin half the size of the bottom margin...
  ;; ; make side margins equal.. two-thirds of the bottom margin. Change the margins to suit your needs.
  (inset pct l t r b))

(define (add-frme pct #:pad (pad 5) (seg #f)(clr "gray")(wdt #f))
  (frame (inset pct pad) #:segment seg #:color clr #:line-width wdt))
;; (pan-spot-view (add-frme #:pad 0 (add-mrgns (text "知足")) 5 "red" 5)) ; ok! 

(define (pict->png-bg pct fnm (clr "white") (inst 10))
  (save-pict fnm ; FileName
             (add-bg (inset pct inst) clr)))

(define (make-spacer-blank choice-pcts)
  (define w (pict-width (argmax pict-width choice-pcts)))
  (define h (pict-height (argmax pict-height choice-pcts)))
  (blank w h))

;;; utilities developed during nuclear-sequence-process-product-waste.rkt
(define (p-bt str) (text str 'bold))
(define (p-it str) (text str 'italic))

(define (place-in-shape pct (shpe filled-ellipse) (btm-mrgn 5) (inst 5)  (clr "Chartreuse") (brdr-clr "Medium Aquamarine") (brdr-wdth 5))
  (define inset-pct (inset pct inst inst inst btm-mrgn))
  (define-values (p-w p-h) (values (pict-width inset-pct) (pict-height inset-pct)))
   (cc-superimpose (shpe p-w p-h #:color clr #:border-color brdr-clr #:border-width brdr-wdth) inset-pct))
;;;;;; utilities -above - move from nuclear-sequence-process-product-waste to pict-procedres.rkt

;;;  ruby, Kanji and Furigana  from views, common, developed for KDHG
;;;   Kawahara-san's Toroku 鉱毒被害メカニズム、borrow from quiz-slide-racket;;;;
(define  kanji-size (make-parameter 36)) ;; slideshow (current-font-size)
(define  furig-size (make-parameter (ceiling (/ (kanji-size) 2.5))))
(define furig-style (make-parameter "IPA Pゴシック"))
(define-values (inst-1 inst-t inst-r inst-b) (values 5 5 2 2))
(define kanji-style (make-parameter "IPAexゴシック"))
(define kanji-furi-gap (make-parameter 3))

(define (fgn->pct fg (sze furig-size)(stl furig-style))
  (inset (text fg (furig-style) (furig-size))  inst-1 inst-t inst-r inst-b))
;; (fgn->pct "ちそく") ; ok!
(define (knj->pct kj (sze kanji-size) (stl kanji-style))
  (text kj (kanji-style) (kanji-size)))
(define (ki-fa->pct k f (appnd vl-append) (gp (kanji-furi-gap)))
  (appnd gp  (fgn->pct f) (knj->pct k)))

(define (kj-fg-prt->pct part (sty (kanji-style)) (siz (kanji-size))) ;; (fga-sze furig-size)... etc
  (cond ;; (raise-argument-error 'kj-fg-prt "string or '(\"知\" \"そく\") pair" part)
    ((pair? part) (ki-fa->pct (first part) (second part)))
    (#t (text part sty siz))))
(define (kj-fg-mrkp-str->pct str (sty (kanji-style))(siz (kanji-size)))
  (apply hbl-append
         (map (lambda (str) (kj-fg-prt->pct str sty siz)) (get-kj-fg-parts str))))

(define (kj-fg-mrkp-comma-stack->pct str (appnd vl-append) (sty (kanji-style)) (siz (kanji-size)))
  (cond
   ((and (comma-ja-check str) (comma-check str))
    (apply appnd
	   (map (lambda (s) (kj-fg-mrkp-str->pct s sty siz))
		(comma-split str))))
   ((semicol-check str)
    (apply appnd
	   (map (lambda (s) (kj-fg-mrkp-str->pct s sty siz))
		(semicol-split str))))
   (#t (kj-fg-mrkp-str->pct str sty siz))))

;;; syntax : pict naming
;;; from racket-picture-showing and Imo, SeiButsu-Bunrui slideshows
(define-syntax-parser  name-picts-n-list
  [(_  names picts list-name)
   #'(begin
       (match-define names picts)
       (define list-name names))])
;; example use from SeiButsu-noBunRui.rkt
#;(name-picts-n-list (list s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11 s12)
		   (map launder (make-list 12 answer-slot))
                   slot-picts)

;; from other common/procedures.. probbly quiz-slide-racket and views
(define-syntax-parser name-txt->pct
  [(_ name txt)
   #:with nme (format-id #'name "~a" #'name)
   #'(define nme (text txt))]
  [(_ name txt pproc)
   #:with nme (format-id #'name "~a" #'name)
   #'(define nme (pproc txt))])

(define-syntax-parser nme-txt-lst->pct
  [(_ lst)
   #:with nme (format-id #'lst "~a" (car (cdr   (syntax->list #'lst))))
   #'(define nme (kj-fg-mrkp-str->pct (syntax-e (caddr (syntax->list #'lst)))) )]
  [(_ lst sze) ;; too involved: need math to adjust fgn->pct, knj->pct, ki-fa->pct sizes
   #:with nme (format-id #'lst "~a" (car (cdr   (syntax->list #'lst))))
   #'(define nme (kj-fg-mrkp-str->pct (syntax-e (caddr (syntax->list #'lst))) (kanji-style) sze))]
  ); end syntax-parser nme-txt-lst->pict
