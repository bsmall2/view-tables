import pandas as pd
# https://stackoverflow.com/questions/77743944/how-do-i-save-beautifulsoup-web-scraped-table-to-csv-file
# instead of Beautiful soup or R's rvest or tidyverse??

# https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html#io-read-html

url = 'https://en.wikipedia.org/wiki/List_of_countries_by_English-speaking_population'
# 'https://www.w3schools.com/html/html_tables.asp' # 
df = pd.read_html(url, match='India')
                  # attrs={"class": "sortable"})
# [0] is the first table in the page

df[0].to_csv('wikipedia-page-table-match.csv', index=False)

# len(df)
# https://www.statology.org/pandas-read-html/

# https://www.dataquest.io/blog/python-vs-r/
# In Python, matplotlib is the primary plotting package, and seaborn is a widely used layer over matplotlib.

# https://www.r-bloggers.com/2019/07/beautifulsoup-vs-rvest/
