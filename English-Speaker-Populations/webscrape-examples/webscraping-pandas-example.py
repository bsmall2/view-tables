import pandas as pd
# https://stackoverflow.com/questions/77743944/how-do-i-save-beautifulsoup-web-scraped-table-to-csv-file
# instead of Beautiful soup or R's rvest or tidyverse??

url = 'https://www.w3schools.com/html/html_tables.asp'
df = pd.read_html(url)[0]  # [0] is the first table in the page

df.to_csv('table.csv', index=False)
