
Military Spending Comparison

| Country      | Global % |
| United States |     40.5 |
| Rest of Nato  |     17.3 |
| China        |      10 |
| Russia       |      4.8 |
| Others       |     27.4 |

 - https://www.washingtonpost.com/world/2024/02/13/nato-spending-russia-ukraine-trump/
