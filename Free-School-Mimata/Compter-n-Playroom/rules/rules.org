#+TITLE: 三股よる学校 PC&Playroom ルール
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>

* 2024年  4月  3日

** Computer and Playroom Rules: ルール
  1. 年齢対象外のゲームはしません。
  2. 課金はしません。
  3. 気もちより言葉を使いましょう。

** Times 時間割

| 始まり |   終り | 内容          |
|-------+-------+--------------|
|  18:30 |  19:00 | 一            |
|  19:00 |  19:10 | ラジオ体操     |
|  19:10 |  19:45 | 二            |
|  19:45 | 何時か | 片づけ、外遊び |


