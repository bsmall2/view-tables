
** Cholera in 2008, privatizatio break 50 year C-free record
 - https://news.un.org/en/story/2023/02/1133337 an exponential rise in cholera cases
#+begin_quote
conflict and extreme climatic events are worsening the triggers of cholera and increasing its toll on lives,”

...

The average case fatality ratio is almost at three per cent, which is above the 2.3 per cent reached in 2022 and far exceeds the acceptable level of below one. 

“It’s critical for African countries to scale up readiness to quickly detect cases and mount comprehensive and timely response,” said Dr. Moeti.

...

“Every death due to cholera is preventable,” said Dr Moeti. “This disease is as much a health challenge as it is a development one. As such investments in better sanitation and access to safe water, formidably complement the public health initiatives to sustainably control and end cholera.”

#+end_quote
 - https://www.thesouthafrican.com/news/breaking-cholera-south-africa-death-toll-rises-to-31-ramaphosa-hammanskraal-8-june-2023/
 - https://www.unicef.org/southafrica/stories/race-against-time-contain-cholera-outbreak-and-save-lives
#+begin_quote
“A cleaner stream can better manage pollution and heal itself even if there is a dose of sewage or pollution but once a stream goes past a tipping point it can’t clean itself,” explained Dr. Mark Graham from GroundTruth, speaking to UNICEF in late 2022.

The quality of water from streams, such as Mthinzima in KwaZulu-Natal is critical as they ultimately flow into reservoirs and provide an essential resource for communities. Keeping water safe, as opposed to cleaning polluted water, can save large sums of money.
#+end_quote

  - https://pubmed.ncbi.nlm.nih.gov/14758861/
 #+begin_quote
 Water privatization programs in South Africa, part of a government policy aimed at making people pay for the full cost of running water ("total cost recovery"), was developed by private water companies and the World Bank to finance improved water supplies and build the country's economy. Instead the programs are causing more misery than development. Millions of poor people have had their water supply cut off because of inability to pay, forcing them to get their water from polluted rivers and lakes and leading to South Africa's worst cholera outbreak--which the government paid millions of dollars to control. Residents in some townships are rebelling, and many of the private multinational water companies are reassessing their involvement in South Africa.
 #+end_quote
  - https://mronline.org/2009/02/16/the-disease-of-privatization/ 
#+begin_quote
... the cholera outbreak in South Africa was not related to the one that had occurred in Zimbabwe.  Rather, the outbreak was linked to poor sanitation services and a lack of access to clean water.3  Nonetheless, the Department of Health was not willing to go any further and discuss the underlying reasons why, fifteen years after apartheid, people still don’t have toilets or clean drinking water.  Of course, the real reasons for this dire situation which the Health Department is loathe to discuss is that the ANC government has completely failed to address the inequalities of apartheid and have rather embarked on the privatization of water and sanitation services.

...

the apartheid regime forced millions of people to live in appalling conditions in townships and homelands, where the regime provided very few services.  There, most people had only shacks for shelter and only the lucky few had electricity — worse, very few townships had refuse removal services and only a few townships had clean water.5  It’s no surprise that cholera outbreaks regularly occurred in the townships and homelands during the apartheid era.  Meanwhile, the white population lived in very well serviced neighborhoods with ample water for their swimming pools, regular refuse removal services that carted away the waste of their indulgent consumer culture, and subsidized electricity that fuelled one of the highest living standards in the world.

...

By the mid-1980s, however, communities were fighting this unjust system across the country, and some of them managed to win some gains, including access to free water.  Besides, the apartheid government was also embarrassed by the international attention that some of the larger cholera outbreaks stirred.  As a result, it began providing some free water in areas where the cholera outbreaks had occurred, including in a number of districts in KwaZulu-Natal.

Hollow Promises

Before it came to power in 1994, the ANC regularly condemned the poor living conditions under which people were forced to live.  The party also promised that once it was in power it would address the inequalities of apartheid and eradicate the diseases caused by poor living conditions, such as cholera, which affected millions people.  To do so, the ANC promised, among other things, to deliver free water and adequate housing to all.  It was on this basis that millions of people voted for the ANC in 1994.

Unfortunately, it became clear from early on that the ANC, like all political parties, had lied to the people and had no intention of living up to its promises.  The Reconstruction and Development Programme (RDP) that the ANC unveiled in 1994 already contained many neo-liberal measures.7  A case in point: with the RDP the ANC committed itself to fiscal austerity, economic deregulation, trade liberalization, and an independent Reserve Bank — all in the name of creating an environment favorable to the expansion of the private sector.  In 1996, the ANC, having shifted even further rightwards, launched the unmistakably pro-corporate economic policy, the toxic Growth Employment and Redistribution Policy (GEAR).8  **GEAR siphoned money into the hands of the elite even more efficiently than apartheid.**

...

... the elite suburb of Durbanville in Cape Town, which has a population of 35,000, receives four times more money per person for waste removal than the township of Khayelitsha, which has a population of 450,000.  Eight times more money is moreover spent per person on providing water to Durbanville residents when compared to Khayelitsha.14  Similarly, in Johannesburg, almost 30% of all residents still live in shacks and 52% of people still have inadequate sanitation services.  In contrast, its rich suburbs, such as Sandton, have one of the highest rates of water consumption in the world.
To top it off, the ANC also passed a national law to prevent any form of progressive cross-subsidizations of services.16
At the same time, the ANC adopted a full cost recovery policy for public and social services.  Translation: people who can’t afford to pay have their services cut.  Consequently, historical inequalities are simply not being addressed, and millions of people in the townships are still being forced to live in an environment that produces cholera and diarrhea.


#+end_quote


